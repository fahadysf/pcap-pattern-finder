# pcap-pattern-finder

Utility to find repeating patterns in PCAP files to help define signatures for unknown protocols.